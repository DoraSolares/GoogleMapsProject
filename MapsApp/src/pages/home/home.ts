import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var google: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapRef: ElementRef;

  constructor(public navCtrl: NavController) {}

    ionViewDidLoad(){
      this.showMap();
    }

    showMap()
    {
      //Location - lat long
      var location = new google.maps.LatLng(-17.391729, -66.155049);
      //Map options
      const options = {
      center: location,
      zoom:15,
      streetViewControl: false,
      mapTypeId: 'roadmap'
    };
    
    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    var directionsDisplay;
   var directionsService = new google.maps.DirectionsService();
   var lat1, lng1;
   var lat2, lng2;
    this.addMarker(location, map);
    this.addMarker2(location, map);
    function calcRoute() {
                var start = new google.maps.LatLng(lat1, lng1);
                var end = new google.maps.LatLng(lat2, lng2);
                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    }
                });
            } 
  }
  
  addMarker(position, map)
  {
    var lat1, lng1
    return new google.maps.Marker({
      position,
      map,
      draggable:true,
      position1: {
        lat: lat1,
        lng: lng1
      }
    });

  }

  addMarker2(position, map)
  {
    var lat2, lng2
    return new google.maps.Marker({
      position,
      map,
      draggable:true,
      position1: {
        lat: lat2,
        lng: lng2
      }
    });

  }
}
